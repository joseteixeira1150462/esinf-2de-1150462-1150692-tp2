/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package txtparser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import lists.GrafoCidade;
import lists.ListaPaises;
import lists.ListaUtilizadores;
import model.Cidade;
import model.Pais;
import model.Utilizador;

/**
 *
 * @author 1150462 && 1150692 
 */
public class CountriesTextFile {
    
     public static void countriesFile(String lineSeparator,
            GrafoCidade graph, ListaPaises listPais) throws IOException {

        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("TXT Files", "TXT");
        chooser.setFileFilter(filter);
        chooser.setDialogTitle("Ficheiro sCountries");
        int returnVal = chooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            System.out.println("You choose to open this file: "
                    + chooser.getSelectedFile().getName());
        }

        try (BufferedReader br = new BufferedReader(new FileReader(chooser.getSelectedFile().getAbsolutePath()))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
               String[] lines = sCurrentLine.toString().split(lineSeparator);

               Cidade cidade = new Cidade(lines[3].trim(),Double.parseDouble(lines[2].trim()),Double.parseDouble(lines[4].trim()),Double.parseDouble(lines[5].trim()));
               Pais pais = new Pais(lines[0].trim(),lines[1].trim(),cidade);
               
               listPais.adicionarEntry(pais);
               
               graph.adicionarCidade(pais.getCidade());

            }
        } catch (LineException e) {
            e.getMessage();
        }
    }
    
}
