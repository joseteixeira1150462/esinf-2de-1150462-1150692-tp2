/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package txtparser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import lists.GrafoAmizade;
import lists.GrafoCidade;
import lists.ListaPaises;
import lists.ListaUtilizadores;
import model.Cidade;
import model.Distancia;
import model.Pais;
import model.Utilizador;
import utils.Utils;

/**
 *
 * @author 1150462 && 1150692 
 */
public class RelationsTextFile {

    public static void relationsFile(String lineSeparator,
            GrafoAmizade graph, ListaUtilizadores listaUtilizador, ListaPaises listaPais) throws IOException {

        Utils u = new Utils();
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("TXT Files", "TXT");
        chooser.setFileFilter(filter);
        chooser.setDialogTitle("Ficheiro sRelationships");
        int returnVal = chooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            System.out.println("You choose to open this file: "
                    + chooser.getSelectedFile().getName());
        }

        try ( BufferedReader br = new BufferedReader(new FileReader(chooser.getSelectedFile().getAbsolutePath()))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                String[] lines = sCurrentLine.toString().split(lineSeparator);

                Utilizador u1 = graph.getUtilizador(lines[0].trim());
                Utilizador u2 = graph.getUtilizador(lines[1].trim());
  
                if (u1 != null && u2 != null) {
                    Cidade cidadeU1 = listaPais.getCidade(u1.getCidade());
                    Cidade cidadeU2 = listaPais.getCidade(u2.getCidade());

                    if (cidadeU1 != null && cidadeU2 != null) {

                        double distancia = u.getDistanciaDuasCoordenadas(cidadeU1.getLatitude(),
                                cidadeU1.getLongitude(),
                                cidadeU2.getLatitude(),
                                cidadeU2.getLongitude());
                        Distancia d = new Distancia(distancia);

                        graph.inserirRelacao(u1, u2, d);

                    } else {
                        System.out.println("Um ou os utilizadores não tem a cidade: " + lines[0].trim() + " / " + lines[1].trim());
                    }

                } else {
                    System.out.println("Um ou os utilizadores: " + lines[0].trim() + " / " + lines[1].trim() + " não existem");

                }
            }
        } catch (LineException e) {
            e.getMessage();
        }
    }

}
