package txtparser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import lists.GrafoCidade;
import lists.ListaPaises;
import model.Cidade;
import model.Distancia;
import model.Pais;
import utils.Utils;

/**
 *
 * @author 1150462 && 1150692 
 */
public class BordersTextFile {
    
    public static void bordersFile(String lineSeparator,
            GrafoCidade graph, ListaPaises listaPais) throws IOException {
        
        Utils u = new Utils();
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("TXT Files", "TXT");
        chooser.setFileFilter(filter);
        chooser.setDialogTitle("Ficheiro sBorders");
        int returnVal = chooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            System.out.println("You choose to open this file: "
                    + chooser.getSelectedFile().getName());
        }

        try (BufferedReader br = new BufferedReader(new FileReader(chooser.getSelectedFile().getAbsolutePath()))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                String[] lines = sCurrentLine.toString().split(lineSeparator);

                Pais pais1 = listaPais.getPais(lines[0].trim());
                Pais pais2 = listaPais.getPais(lines[1].trim());

                if (pais1 == null || pais2 == null) {
                    System.out.println("Os países não existem!");
                } else {
                    double distancia = u.getDistanciaDuasCoordenadas(pais1.getCidade().getLatitude(),
                            pais1.getCidade().getLongitude(),
                            pais2.getCidade().getLatitude(),
                            pais2.getCidade().getLongitude());
                    Distancia d = new Distancia(distancia);
                    graph.adicionarFronteira(pais1.getCidade(), pais2.getCidade(), d, d.getDistancia());
                }
                
            }
        } catch (LineException e) {
            e.getMessage();
        }
        
    }

}
