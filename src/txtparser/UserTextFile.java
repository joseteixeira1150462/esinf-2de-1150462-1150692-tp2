package txtparser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import lists.GrafoAmizade;
import lists.ListaUtilizadores;
import model.Utilizador;

/**
 *
 * @author 1150462 && 1150692 
 */
public class UserTextFile extends LinkedList<Utilizador> {

    public UserTextFile() {

    }

    public static void userfile(String lineSeparator,
            GrafoAmizade graph, ListaUtilizadores listUser) throws IOException {

        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("TXT Files", "TXT");
        chooser.setFileFilter(filter);
        chooser.setDialogTitle("Ficheiro sUsers");
        int returnVal = chooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            System.out.println("You choose to open this file: "
                    + chooser.getSelectedFile().getName());
        }

        try (BufferedReader br = new BufferedReader(new FileReader(chooser.getSelectedFile().getAbsolutePath()))) {
            String sCurrentLine;
            Utilizador u = new Utilizador();
            while ((sCurrentLine = br.readLine()) != null) {
                String[] lines = sCurrentLine.toString().split(lineSeparator);
                

                u = new Utilizador(lines[0].trim(), Integer.parseInt(lines[1].trim()), lines[2].trim());
                
                listUser.adicionarUtilizador(u);
                graph.inserirUtilizador(u);
                

            }
        } catch (LineException e) {
            e.getMessage();
        }
        
    }
}
