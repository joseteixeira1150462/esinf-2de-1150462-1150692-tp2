package lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import model.Utilizador;
import matrixgraph.AdjacencyMatrixGraph;
import model.Distancia;
import utils.DoublyLinkedList;
import utils.Pair;

/**
 *
 * @author 1150462 && 1150692 
 */
public class GrafoAmizade {

    /**
     * Grafo em que cada V é um utilizador e a Edge uma matriz de adjacencias da
     * relação entre utilizadores*
     */
    private AdjacencyMatrixGraph<Utilizador, Distancia> graphAmizade;

    public GrafoAmizade() {
        graphAmizade = new AdjacencyMatrixGraph<Utilizador, Distancia>();
    }
    
    public AdjacencyMatrixGraph<Utilizador, Distancia> getGrafoAmizade() {
        return this.graphAmizade;
    }
    
    public boolean inserirUtilizador(Utilizador u) {
        return graphAmizade.insertVertex(u);
    } 
    
    public boolean inserirRelacao(Utilizador u1, Utilizador u2, Distancia d) {
        return graphAmizade.insertEdge(u1, u2, d);
    }
    
    public Utilizador getUtilizador(String nome) {
        
        for (Utilizador utilizador : graphAmizade.vertices()) {
            if (nome.equals(utilizador.getNome())){
                return utilizador;
            }
        }
        
        return null;
    }
    
    public DoublyLinkedList<Utilizador> getUsersOrderedByPopularity() {
        List<Pair<Integer, Utilizador>> sortedUsersByPopularity = new ArrayList<Pair<Integer, Utilizador>>();

        for (Utilizador u : graphAmizade.vertices()) {
            Integer numeroAmigos = graphAmizade.outDegree(u);
            
            sortedUsersByPopularity.add(Pair.of(numeroAmigos, u));
        }

        Collections.sort(sortedUsersByPopularity, new Comparator<Pair<Integer, Utilizador>>() {
            @Override
            public int compare(Pair<Integer, Utilizador> o1, Pair<Integer, Utilizador> o2) {
                return o1.compareTo(o2);
            }
        });

        Collections.reverse(sortedUsersByPopularity);
        DoublyLinkedList<Utilizador> utilizadoresOrdenados = new DoublyLinkedList<>();

        for (Pair pair : sortedUsersByPopularity) {
            utilizadoresOrdenados.addLast((Utilizador) pair.second);
        }
        

        return utilizadoresOrdenados;
    }
    
    
}
