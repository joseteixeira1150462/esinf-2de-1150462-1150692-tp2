package lists;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import model.Cidade;
import model.Pais;

/**
 *
 * @author 1150462 && 1150692 
 */
public class ListaPaises {

    private List<Pais> listaPaises;

    public ListaPaises() {
        listaPaises = new ArrayList<>();
    }

    public List<Pais> getListaPaises() {
        return listaPaises;
    }
    
    public void adicionarEntry(Pais p) {
        this.listaPaises.add(p);
    }

    public Pais getPais(String nomePais) {
        for (Pais pais : listaPaises) {
            if (nomePais.equals(pais.getNomePais())) {
                return pais;
            }
        }
        
        return null;
    }
    
    public Cidade getCidade(String nomeCidade) {
        for (Pais p : listaPaises) {
            if(nomeCidade.equals(p.getCidade().getNome())) {
                return p.getCidade();
            }
        }
        
        return null;
    }
}
