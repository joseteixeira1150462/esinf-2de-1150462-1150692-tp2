package lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import mapgraph.Graph;
import mapgraph.GraphAlgorithms;
import model.Cidade;
import model.Distancia;
import utils.Pair;

/**
 *
 * @author 1150462 && 1150692 
 */
public class GrafoCidade {

    /**
     * Um grafo cujos vértices sejam as capitais dos Paises e as Edges um mapa
     * de adjacencia entre fronteiras de Paises 
     */
    private Graph<Cidade,Distancia> graphCidade;
    
    public GrafoCidade(boolean directed){
        graphCidade = new Graph(directed);
    }

    public Graph<Cidade, Distancia> getGraphCidade() {
        return graphCidade;
    }
    
    public boolean adicionarCidade(Cidade c) {
        return graphCidade.insertVertex(c);
    }
    
    public boolean adicionarFronteira(Cidade c1, Cidade c2, Distancia d, Double distancia) {
        return graphCidade.insertEdge(c1, c2, d, distancia);
    }
    
    public List<Cidade> getCityAtNBorders(int numeroFronteiras, Cidade cidadeUtilizador) {
        List<Cidade> listaCidades = new LinkedList<>();
        
        for(Cidade c : graphCidade.allKeyVerts()) {
            
            ArrayList<LinkedList<Cidade>> caminhosAllCidades = mapgraph.GraphAlgorithms.allPaths(graphCidade, cidadeUtilizador, c);

            
            if (caminhosAllCidades.size() > 0) {
                for (LinkedList<Cidade> caminhosAllCidade : caminhosAllCidades) {
                    if (caminhosAllCidade.size() - 1 <= numeroFronteiras && !listaCidades.contains(c)) {
                        listaCidades.add(c);
                    }
                }
            }

        }

        return listaCidades;
    }
    
    public LinkedList<Cidade> getCitiesOrderByDistanceAverage() {
        List<Pair<Double, Cidade>> sortedCitiesByDistanceAverage = new ArrayList<Pair<Double, Cidade>>();

        for (Cidade c : graphCidade.vertices()) {
            int totalCidadeDistancia = 0;
            double media = 0;
            int totalCidades = 0;
            ArrayList<LinkedList<Cidade>> paths = new ArrayList<>();
            ArrayList<Double> dists = new ArrayList<>();

            if (GraphAlgorithms.shortestPaths(graphCidade, c, paths, dists)) {
                for (int i = 0; i < paths.size(); i++) {

                    LinkedList<Cidade> path = paths.get(i);
                    for (int j = 0; j < path.size(); j++) {
                        totalCidadeDistancia += dists.get(i);
                        totalCidades++;
                    }
                }


                media = totalCidadeDistancia / totalCidades;
            }

            sortedCitiesByDistanceAverage.add(Pair.of(media, c));
        }

        Collections.sort(sortedCitiesByDistanceAverage, new Comparator<Pair<Double, Cidade>>() {
            @Override
            public int compare(Pair<Double, Cidade> t, Pair<Double, Cidade> t1) {
                return t.compareTo(t1);
            }
        });

        LinkedList<Cidade> cidadesOrdenadas = new LinkedList<>();

        for (Pair p : sortedCitiesByDistanceAverage) {
            cidadesOrdenadas.addLast((Cidade) p.second);
        }
        return cidadesOrdenadas;
    }
      
}
