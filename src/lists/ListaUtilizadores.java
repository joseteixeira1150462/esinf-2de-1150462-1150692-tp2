package lists;

import java.util.LinkedList;
import model.Utilizador;

/**
 *
 * @author 1150462 && 1150692 
 */
public class ListaUtilizadores {

    LinkedList<Utilizador> listaUser;

    public ListaUtilizadores() {
        this.listaUser = new LinkedList<>();
    }

    public LinkedList<Utilizador> getListaUser() {
        return listaUser;
    }

    public void adicionarUtilizador(Utilizador u) {
        if (!this.listaUser.contains(u)) {
            listaUser.add(u);
        }
    }
    
    public Utilizador getUtilizador(String nome) {
        for (Utilizador utilizador : listaUser) {
            if (nome.equals(utilizador.getNome())){
                return utilizador;
            }
        }
        
        return null;
    }
 
}
