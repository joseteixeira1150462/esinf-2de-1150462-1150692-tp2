package model;

import java.util.Objects;

/**
 *
 * @author 1150462 && 1150692 
 */
public class Utilizador implements Comparable<Utilizador> {

    private String nome;
    private int idade;
    private String cidade;

    private static final String NOME_POR_OMISSAO = "S/NOME";
    private static final int IDADE_POR_OMISSAO = 0;
    private static final String CIDADE_POR_OMISSAO = "S/CIDADE";

    public Utilizador(String nome, int idade, String cidade) {
        this.nome = nome;
        this.idade = idade;
        this.cidade = cidade;
    }

    public Utilizador(String nome) {
        this.nome = nome;
        this.idade = IDADE_POR_OMISSAO;
        this.cidade = CIDADE_POR_OMISSAO;
    }

    public Utilizador() {
        this.nome = NOME_POR_OMISSAO;
        this.idade = IDADE_POR_OMISSAO;
        this.cidade = CIDADE_POR_OMISSAO;
    }

    public String getNome() {
        return nome;
    }

    public int getIdade() {
        return idade;
    }

    public String getCidade() {
        return cidade;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.nome);
        hash = 37 * hash + this.idade;
        hash = 37 * hash + Objects.hashCode(this.cidade);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Utilizador other = (Utilizador) obj;
        if (this.idade != other.idade) {
            return false;
        }
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.cidade, other.cidade)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Utilizador o) {
        return this.nome.compareTo(o.nome);
    }

    @Override
    public String toString() {
        return "Utilizador{" + "nome=" + nome + ", idade=" + idade + ", cidade=" + cidade + '}';
    }
}
