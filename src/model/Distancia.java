/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;

/**
 *
 * @author 1150462 && 1150692 
 */
public class Distancia {

    private Double distancia;

    private final static Double DISTANCIA_POR_OMISSAO = 0.0;

    public Distancia(Double distancia) {
        this.distancia = distancia;
    }

    public Distancia() {
        this.distancia = DISTANCIA_POR_OMISSAO;

    }

    public Double getDistancia() {
        return distancia;
    }

    public void setDistancia(Double distancia) {
        this.distancia = distancia;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.distancia);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Distancia other = (Distancia) obj;
        if (!Objects.equals(this.distancia, other.distancia)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Distancia{" + "distancia=" + distancia + '}';
    }

}
