/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;

/**
 *
 * @author 1150462 && 1150692 
 */
public class Cidade implements Comparable<Cidade> {
    
    private String nome;
    private Double populacao;
    private Double latitude;
    private Double longitude;
    
    private static final String NOME_PAIS_POR_OMISSAO = "S/NOME_PAÍS";
    private static final Double POPULACAO_POR_OMISSAO = 0d;
    private static final Double LATITUDE_POR_OMISSAO = 0d;
    private static final Double LONGITUDE_POR_OMISSAO = 0d;

    
    public Cidade(String nome, Double populacao, Double latitude, Double longitude) {
        this.nome = nome;
        this.populacao = populacao;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Cidade() {
        this.nome = NOME_PAIS_POR_OMISSAO;
        this.populacao = POPULACAO_POR_OMISSAO;
        this.latitude = LATITUDE_POR_OMISSAO;
        this.longitude = LONGITUDE_POR_OMISSAO;
    }
    
    
    
    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return the populacao
     */
    public Double getPopulacao() {
        return populacao;
    }

    /**
     * @return the latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * @return the longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @param populacao the populacao to set
     */
    public void setPopulacao(Double populacao) {
        this.populacao = populacao;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.nome);
        hash = 53 * hash + Objects.hashCode(this.populacao);
        hash = 53 * hash + Objects.hashCode(this.latitude);
        hash = 53 * hash + Objects.hashCode(this.longitude);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cidade other = (Cidade) obj;
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.populacao, other.populacao)) {
            return false;
        }
        if (!Objects.equals(this.latitude, other.latitude)) {
            return false;
        }
        if (!Objects.equals(this.longitude, other.longitude)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cidade{" + "nome=" + nome + '}';
    }

    public String toStringTudo() {
        return "Cidade{" + "nome=" + nome + ", populacao=" + populacao + ", latitude=" + latitude + ", longitude=" + longitude + '}';
    }

    @Override
    public int compareTo(Cidade o) {
        return this.nome.compareTo(o.nome);
    }
 
}
