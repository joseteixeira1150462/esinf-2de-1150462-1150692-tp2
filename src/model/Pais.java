package model;

import java.util.Objects;

/**
 *
 * @author 1150462 && 1150692 
 */
public class Pais implements Comparable<Pais> {

    private String nomePais;
    private String nomeContinente;
    private Cidade cidade;

    private static final String NOME_PAIS_POR_OMISSAO = "S/NOME_PAÍS";
    private static final String NOME_CONTINENTE_POR_OMISSAO = "S/NOME_CONTINENTE";    private static final float LATITUDE_POR_OMISSAO = 0F;
    private static final Cidade CIDADE_POR_OMISSAO = new Cidade();
    
    public Pais(String nomePais, String nomeContinente, Cidade cidade) {
        this.nomePais = nomePais;
        this.nomeContinente = nomeContinente;
        this.cidade = cidade;
    }

    public Pais() {
        this.nomePais = NOME_PAIS_POR_OMISSAO;
        this.nomeContinente = NOME_CONTINENTE_POR_OMISSAO;
        this.cidade = CIDADE_POR_OMISSAO;
    }
 
    public String getNomePais() {
        return nomePais;
    }

    public void setNomePais(String nomePais) {
        this.nomePais = nomePais;
    }

    public String getNomeContinente() {
        return nomeContinente;
    }

    public void setNomeContinente(String nomeContinente) {
        this.nomeContinente = nomeContinente;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + Objects.hashCode(this.nomePais);
        hash = 43 * hash + Objects.hashCode(this.nomeContinente);
        hash = 43 * hash + Objects.hashCode(this.cidade);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pais other = (Pais) obj;
        if (!Objects.equals(this.nomePais, other.nomePais)) {
            return false;
        }
        if (!Objects.equals(this.nomeContinente, other.nomeContinente)) {
            return false;
        }
        if (!Objects.equals(this.cidade, other.cidade)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Pais{" + "nomePais=" + nomePais + '}';
    }
    
    public String toStringTudo() {
        return "Pais{" + "nomePais=" + nomePais + ", nomeContinente=" + nomeContinente + ", cidade=" + cidade + '}';
    }
    
    @Override
    public int compareTo(Pais o) {
        return this.nomePais.compareTo(o.nomePais);
    }

}
