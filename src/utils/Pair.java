package utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author 1150462 && 1150692
 */
public class Pair<U extends Comparable<U>, V extends Comparable<V>> implements Comparable<Pair<U, V>> {

    public U first;       // first field of a Pair
    public V second;      // second field of a Pair

    // Constructs a new Pair with specified values
    private Pair(U first, V second) {
        this.first = first;
        this.second = second;
    }

    public V getSecond() {
        return this.second;
    }

    @Override
    // Checks specified object is "equal to" current object or not
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Pair<?, ?> pair = (Pair<?, ?>) o;

        // call equals() method of the underlying objects
        if (!first.equals(pair.first)) {
            return false;
        }
        return second.equals(pair.second);
    }

    @Override
    // Computes hash code for an object to support hash tables
    public int hashCode() {
        // use hash codes of the underlying objects
        return 31 * first.hashCode() + second.hashCode();
    }

    @Override
    public String toString() {
        return "(" + first + ", " + second + ")";
    }

    // Factory method for creating a Typed Pair immutable instance
    public static <U extends Comparable<U>, V extends Comparable<V>> Pair<U, V> of(U a, V b) {
        // calls private constructor
        return new Pair<>(a, b);
    }

    @Override
    public int compareTo(Pair<U, V> o) {
        if (this.first.compareTo(o.first) == 0) {
            return this.second.compareTo(o.second);
        } else {
            return this.first.compareTo(o.first);
        }
    }

}
