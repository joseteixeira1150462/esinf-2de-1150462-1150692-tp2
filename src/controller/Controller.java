package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;
import lists.GrafoAmizade;
import lists.GrafoCidade;
import lists.ListaPaises;
import lists.ListaUtilizadores;
import model.Cidade;
import model.Utilizador;
import utils.DoublyLinkedList;
import utils.Pair;

/**
 *
 * @author 1150462 && 1150692
 */
public class Controller {

    private GrafoCidade graphCidade;
    private GrafoAmizade graphAmizade;
    private ListaPaises lp;
    private ListaUtilizadores lu;

    public Controller(GrafoCidade graphCidade, GrafoAmizade graphAmizade, ListaPaises lp, ListaUtilizadores lu) throws IOException {

        this.graphAmizade = graphAmizade;
        this.graphCidade = graphCidade;
        this.lp = lp;
        this.lu = lu;
        readUsers();
        readCities();
        readRelations();
        readDistanceCities();
    }

    /**
     * 1) Construir os grafos a partir da informação fornecida nos ficheiros de
     * texto. A capital de um país tem ligação direta com as capitais dos países
     * com os quais faz fronteira. O cálculo da distância em Kms entre duas
     * capitais deverá ser feito através das suas coordenadas.*
     */
    public void readUsers() throws IOException {
        txtparser.UserTextFile.userfile(",", graphAmizade, lu);
    }

    public void readRelations() throws IOException {
        txtparser.RelationsTextFile.relationsFile(",", graphAmizade, lu, lp);
    }

    public void readCities() throws IOException {
        txtparser.CountriesTextFile.countriesFile(",", graphCidade, lp);
    }

    public void readDistanceCities() throws IOException {
        txtparser.BordersTextFile.bordersFile(",", graphCidade, lp);
    }

    /**
     * 2) Devolver os amigos comuns entre os n utilizadores mais populares da
     * rede*
     */
    public List<Utilizador> numberOfSameFriendsPopularUser(Integer numeroMaisPopulares) {

        DoublyLinkedList<Utilizador> utilizadoresPopularesOrdenados = graphAmizade.getUsersOrderedByPopularity();

        ListIterator<Utilizador> itr = utilizadoresPopularesOrdenados.listIterator();

        int count = 2;
        List<Utilizador> sameFriends = new ArrayList<>();

        //Guarda numa lista os amigos em comum com o mais popular e o segundo mais popular
        List<Utilizador> amigosPrimeiro = (ArrayList) graphAmizade.getGrafoAmizade().directConnections(itr.next());
        Utilizador uSecond = itr.next();
        List<Utilizador> amigosSegundo = (ArrayList) graphAmizade.getGrafoAmizade().directConnections(uSecond);
        sameFriends = getSameFriends(amigosPrimeiro, amigosSegundo, count);

        while (count < numeroMaisPopulares && itr.hasNext()) {
            List<Utilizador> amigosUtilizador = (ArrayList) graphAmizade.getGrafoAmizade().directConnections(itr.next());
            sameFriends = getSameFriends(amigosUtilizador, sameFriends, count);
            count++;
        }

        return sameFriends;
    }

    private List<Utilizador> getSameFriends(List<Utilizador> u1, List<Utilizador> u2, int count) {
        List<Utilizador> sameFriends = new ArrayList<>();
        for (int i = 0; i < u1.size(); i++) {
            if (u2.contains(u1.get(i)) && !sameFriends.contains(u1.get(i))) {
                sameFriends.add(u1.get(i));
            } else if (!u2.contains(u1.get(i)) && sameFriends.contains(u1.get(i))) {
                sameFriends.remove(u1.get(i));
            }
        }

        return sameFriends;
    }

    /**
     * 3) Verificar se a rede de amizades é conectada e devolver o número minimo
     * de ligações necessárias para nesta rede todos os utilizadores se
     * comunicarem entre todos*
     */
    public int getMinConnectionsConnected() {

        List<Utilizador> listaUtilizadores = (List) graphAmizade.getGrafoAmizade().vertices();
        int ligacoes = -1;
        LinkedList<Utilizador> shortestPathretorno = new LinkedList<>();
        if (matrixgraph.GraphAlgorithms.isConnected(graphAmizade.getGrafoAmizade(), listaUtilizadores.get(0))) {
            for (Utilizador uOrigem : listaUtilizadores) {
                for (int i = 0; i < listaUtilizadores.size(); i++) {
                    Utilizador uDest = listaUtilizadores.get(i);
                    LinkedList<Utilizador> shortestPath = matrixgraph.GraphAlgorithms.shortestPathBFS(graphAmizade.getGrafoAmizade(), uOrigem, uDest);
                    if (shortestPath.size() - 1 > ligacoes) {
                        ligacoes = shortestPath.size() - 1;
                        shortestPathretorno = shortestPath;
                    }
                }
            }
        }

        return ligacoes;
    }

    /**
     * 4)Devolver para um utilizador os amigos que se encontrem nas proximidades
     * amigos que habitem em cidades que distam um dado número de fronteiras da
     * cidade desse utilizador. Devolver para cada cidade os respetivos amigos.*
     */
    public Map<Cidade, List<Utilizador>> friendsNearByBordersCity(int numeroFronteiras, Utilizador utilizador) {
        Map<Cidade, List<Utilizador>> listaCidadeUtilizadores = new LinkedHashMap<>();
        Cidade cidadeUtilizador = lp.getCidade(utilizador.getCidade());

        List<Cidade> listaCidadeNBorders = graphCidade.getCityAtNBorders(numeroFronteiras, cidadeUtilizador);

        List<Utilizador> listaAmigos = (ArrayList) graphAmizade.getGrafoAmizade().directConnections(utilizador);
        if (listaAmigos != null) {
            for (Utilizador utilizadorAmigo : listaAmigos) {
                Cidade cidadeAmigo = lp.getCidade(utilizadorAmigo.getCidade());

                if (listaCidadeNBorders.contains(cidadeAmigo)) {
                    //adiciona no map
                    if (listaCidadeUtilizadores.keySet().contains(cidadeAmigo)) {
                        listaCidadeUtilizadores.get(cidadeAmigo).add(utilizadorAmigo);
                    } else {
                        listaCidadeUtilizadores.put(cidadeAmigo, new ArrayList<Utilizador>());
                        listaCidadeUtilizadores.get(cidadeAmigo).add(utilizadorAmigo);
                    }
                }
            }
        }
        return listaCidadeUtilizadores;
    }

    /**
     * 5) Devolver as n cidades com maior centralidade,ou seja as cidades que em
     * média estão mais próximas de todas as outras cidades,e com uma
     * percentagem relativa superior a outras cidades.*
     */
    public List<Pair<Cidade, Integer>> centralCitiesOrderByHighPopulationDistanceAverage(Integer p, int n) {

        LinkedList<Cidade> cidadesMaiorMediaDistanciaOrdenadas = graphCidade.getCitiesOrderByDistanceAverage();
        List<Pair<Cidade, Integer>> listaCidadePercentagemUtilizador = new ArrayList<Pair<Cidade, Integer>>();

        int count = 0;
        int i = 0;
        while (count < n) {
            int totalUsersCidade = 0;

            for (Utilizador u : graphAmizade.getGrafoAmizade().vertices()) {
                if (u.getCidade().equals(cidadesMaiorMediaDistanciaOrdenadas.get(i).getNome())) {
                    totalUsersCidade++;
                }
            }
            int percentagem = ((totalUsersCidade * 100 / graphAmizade.getGrafoAmizade().numVertices()));

            if (percentagem > p) {
                listaCidadePercentagemUtilizador.add(Pair.of(cidadesMaiorMediaDistanciaOrdenadas.get(i), percentagem));
                count++;
            }
            i++;
        }

        return listaCidadePercentagemUtilizador;
    }

    /**
     * 6) Devolver o caminho terrestre mais curto entre dois utilizadores,
     * passando obrigatoriamente pelas n cidade(s) intermédias onde cada
     * utilizador tenha o maior número de amigos*
     */
    public Map<List<Cidade>, Double> shortestPathBetweenUsers(int nCidades, Utilizador utilizador1, Utilizador utilizador2) {
        Map<List<Cidade>,Double> pathDistancia = new HashMap<>();
        
        LinkedList<Cidade> shortestPathTemp = new LinkedList<>();
        LinkedList<Cidade> shortestPath = new LinkedList<>();
        double pathSize = 0;
        
        List<Cidade> listaCidadesUtilizador1 = getUserNMostFriendsCities(utilizador1, nCidades);
        List<Cidade> listaCidadesUtilizador2 = getUserNMostFriendsCities(utilizador2, nCidades);
        List<Cidade> listaCidadesIntermedias = listaCidadesUtilizador1;
                       
        for (Cidade cidade2 : listaCidadesUtilizador2) {
            if (!listaCidadesIntermedias.contains(cidade2)) {
                listaCidadesIntermedias.add(cidade2);
            }
        }
              
        Cidade cOrigem = lp.getCidade(utilizador1.getCidade());
        Cidade cDest = lp.getCidade(utilizador2.getCidade());
        
        if (listaCidadesIntermedias.contains(cOrigem)) {
            listaCidadesIntermedias.remove(cOrigem);
        } else if (listaCidadesIntermedias.contains(cDest)) {
            listaCidadesIntermedias.remove(cDest);
        }
         
        for (Cidade cidadeIntermedia : listaCidadesIntermedias) {
            pathSize = pathSize + mapgraph.GraphAlgorithms.shortestPath(graphCidade.getGraphCidade(), cOrigem, cidadeIntermedia, shortestPathTemp);
            
            cOrigem = cidadeIntermedia;
            for (Cidade cidade : shortestPathTemp) {
                shortestPath.add(cidade); 
            }
            shortestPathTemp.clear();
        }

        pathSize = pathSize + mapgraph.GraphAlgorithms.shortestPath(graphCidade.getGraphCidade(), cOrigem, cDest, shortestPathTemp);
        for (Cidade cidade : shortestPathTemp) {
            shortestPath.add(cidade);
        }

        pathDistancia.put(shortestPath, pathSize);
        return pathDistancia;
    }

    private List<Cidade> getUserNMostFriendsCities(Utilizador utilizador, int nCidades) {
        Map<Cidade, Integer> amigosCidade = new TreeMap<>();

        List<Utilizador> listaAmigos = (ArrayList) graphAmizade.getGrafoAmizade().directConnections(utilizador);

        for (Utilizador utilizadorAmigo : listaAmigos) {
            Cidade cidadeAmigo = lp.getCidade(utilizadorAmigo.getCidade());

            if (amigosCidade.keySet().contains(cidadeAmigo)) {
                int nrAmigos = amigosCidade.get(cidadeAmigo) + 1;
                amigosCidade.put(cidadeAmigo, nrAmigos);
            } else {
                amigosCidade.put(cidadeAmigo, 1);
            }
        }

        //Ordenar o map pelos values
        List<Map.Entry<Cidade, Integer>> list
                = new LinkedList<Map.Entry<Cidade, Integer>>(amigosCidade.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<Cidade, Integer>>() {
            public int compare(Map.Entry<Cidade, Integer> o1,
                    Map.Entry<Cidade, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        Map<Cidade, Integer> sortedAmigosCidade = new LinkedHashMap<Cidade, Integer>();
        for (Map.Entry<Cidade, Integer> entry : list) {
            sortedAmigosCidade.put(entry.getKey(), entry.getValue());
        }

        List<Cidade> listaCidades = new LinkedList<>();

        //Retornar só as n cidades com mais amigos
        Iterator itr = sortedAmigosCidade.entrySet().iterator();

        int i = 0;
        while (itr.hasNext() && i < nCidades) {

            Map.Entry<Cidade, Utilizador> entry = (Map.Entry<Cidade, Utilizador>) itr.next();
            listaCidades.add(entry.getKey());
            i++;

        }

        return listaCidades;
    }
}
