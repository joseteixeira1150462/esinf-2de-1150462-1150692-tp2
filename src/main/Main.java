package main;

import controller.Controller;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import lists.GrafoAmizade;
import lists.GrafoCidade;
import lists.ListaPaises;
import lists.ListaUtilizadores;
import model.Cidade;
import model.Utilizador;

/**
 *
 * @author 1150462 && 1150692 
 */
public class Main {

    public static void main(String args[]) throws IOException {

        GrafoAmizade ga = new GrafoAmizade();
        GrafoCidade gc = new GrafoCidade(false);
        ListaUtilizadores lu = new ListaUtilizadores();
        ListaPaises lp = new ListaPaises();

        //Reads Files
        Controller c = new Controller(gc, ga, lp, lu);

//       #2
//      List<Utilizador> lista2 = c.numberOfSameFriendsPopularUser(2);
//      System.out.println(lista2.toString());


//       #3
//        int ligacoes = c.getMinConnectionsConnected();
//        if (ligacoes == -1) {
//            System.out.println("O grafo não é conexo");
//        } else {
//            System.out.println(ligacoes);
//        }

//       #4
//        Utilizador u = new Utilizador("u33", 48, "brasilia");
//        Map<Cidade, List<Utilizador>> lista = c.friendsNearByBordersCity(2, u);
//        System.out.println(lista.toString());

//        //#5
//        List <Pair<Cidade,Integer>> listaPair = c.centralCitiesOrderByHighPopulationDistanceAverage(8, 3);
//        System.out.println(listaPair);

          //#6
          Utilizador utilizador1 = new Utilizador("u1",27,"brasilia");
          Utilizador utilizador2 = new Utilizador("u3",20,"quito");
          Map<List<Cidade>, Double> map6 = c.shortestPathBetweenUsers(1, utilizador1, utilizador2);
          System.out.println(map6.toString());
    }

}
