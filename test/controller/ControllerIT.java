package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import lists.GrafoAmizade;
import lists.GrafoCidade;
import lists.ListaPaises;
import lists.ListaUtilizadores;
import model.Cidade;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import txtparser.BordersTextFile;
import txtparser.CountriesTextFile;
import txtparser.RelationsTextFile;
import txtparser.UserTextFile;
import utils.Pair;

/**
 *
 * @author 1150462 && 1150692
 */
public class ControllerIT {

    private static ListaPaises lpSmallNet;
    private static ListaUtilizadores luSmallNet;
    private static GrafoCidade graphCidadeSmallNet;
    private static GrafoAmizade graphAmizadeSmallNet;
    private static Controller instanceSmallNet;

    private static ListaPaises lpBigNet;
    private static ListaUtilizadores luBigNet;
    private static GrafoCidade graphCidadeBigNet;
    private static GrafoAmizade graphAmizadeBigNet;
    private static Controller instanceBigNet;

    public ControllerIT() {

    }

    @BeforeClass
    public static void setUpClass() throws IOException {
        lpSmallNet = new ListaPaises();
        luSmallNet = new ListaUtilizadores();
        graphCidadeSmallNet = new GrafoCidade(false);
        graphAmizadeSmallNet = new GrafoAmizade();
        instanceSmallNet = new Controller(graphCidadeSmallNet, graphAmizadeSmallNet, lpSmallNet, luSmallNet);
        
        
        
        lpBigNet = new ListaPaises();
        luBigNet = new ListaUtilizadores();
        graphCidadeBigNet = new GrafoCidade(false);
        graphAmizadeBigNet = new GrafoAmizade();
        instanceBigNet = new Controller(graphCidadeBigNet, graphAmizadeBigNet, lpBigNet, luBigNet);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    @Test
    public void testNumberOfSameFriendsPopularUser() {
        //SmallNetwork TEST
        List<Utilizador> expResult = new ArrayList<>();
        expResult.add(new Utilizador("u9", 12, "lapaz"));

        List<Utilizador> expResult2 = new ArrayList<>();
        expResult2.add(new Utilizador("u3", 20, "quito"));
        expResult2.add(new Utilizador("u9", 12, "lapaz"));
        expResult2.add(new Utilizador("u32", 55, "brasilia"));

        List<Utilizador> result = instanceSmallNet.numberOfSameFriendsPopularUser(3);
        List<Utilizador> result2 = instanceSmallNet.numberOfSameFriendsPopularUser(2);

        assertEquals(expResult.toString(), result.toString());
        assertEquals(expResult2.toString(), result2.toString());

        //BigNetwork TEST
        List<Utilizador> expResult3 = new ArrayList<>();
        expResult3.add(luBigNet.getUtilizador("u1"));
        expResult3.add(luBigNet.getUtilizador("u5"));
        expResult3.add(luBigNet.getUtilizador("u30"));
        expResult3.add(luBigNet.getUtilizador("u31"));
        expResult3.add(luBigNet.getUtilizador("u36"));

        List<Utilizador> result3 = instanceBigNet.numberOfSameFriendsPopularUser(2);
        
        assertEquals(expResult3.toString(), result3.subList(0, 5).toString());

    }

    @Test
    public void testGetMinConnectionsConnected() {
        int expResult = 25;
        int expResult2 = -1;
        int result = instanceSmallNet.getMinConnectionsConnected();
        int result2 = instanceBigNet.getMinConnectionsConnected();

        assertTrue(result == 25);
        assertTrue(result2 == -1);

    }

    @Test
    public void testFriendsNearByBordersCity() {
        Map<Cidade, List<Utilizador>> expResult = new LinkedHashMap<>();
        List<Utilizador> listQuito = new ArrayList<>();
        listQuito.add(luSmallNet.getUtilizador("u3"));
        expResult.put(lpSmallNet.getCidade("quito"), listQuito);

        List<Utilizador> listLaPaz = new ArrayList<>();
        listLaPaz.add(luSmallNet.getUtilizador("u9"));
        expResult.put(lpSmallNet.getCidade("lapaz"), listLaPaz);

        List<Utilizador> listBogota = new ArrayList<>();
        listBogota.add(luSmallNet.getUtilizador("u15"));
        listBogota.add(luSmallNet.getUtilizador("u19"));
        listBogota.add(luSmallNet.getUtilizador("u23"));
        expResult.put(lpSmallNet.getCidade("bogota"), listBogota);

        List<Utilizador> listAssuncao = new ArrayList<>();
        listAssuncao.add(luSmallNet.getUtilizador("u16"));
        listAssuncao.add(luSmallNet.getUtilizador("u21"));
        expResult.put(lpSmallNet.getCidade("assuncao"), listAssuncao);

        List<Utilizador> listLima = new ArrayList<>();
        listLima.add(luSmallNet.getUtilizador("u24"));
        expResult.put(lpSmallNet.getCidade("lima"), listLima);

        List<Utilizador> listSantiago = new ArrayList<>();
        listSantiago.add(luSmallNet.getUtilizador("u30"));
        expResult.put(lpSmallNet.getCidade("santiago"), listSantiago);

        List<Utilizador> listBuenosaires = new ArrayList<>();
        listBuenosaires.add(luSmallNet.getUtilizador("u31"));
        expResult.put(lpSmallNet.getCidade("buenosaires"), listBuenosaires);

        List<Utilizador> listBrasilia = new ArrayList<>();
        listBrasilia.add(luSmallNet.getUtilizador("u32"));
        expResult.put(lpSmallNet.getCidade("brasilia"), listBrasilia);

        Map<Cidade, List<Utilizador>> result = instanceSmallNet.friendsNearByBordersCity(2, new Utilizador("u33", 48, "brasilia"));

        assertEquals(expResult.toString(), result.toString());
    }

    @Test
    public void testCentralCitiesOrderByHighPopulationDistanceAverage() {
        System.out.println("testCentralCitiesOrderByHighPopulationDistanceAverage");

        List<Pair<Cidade, Integer>> expResult = new LinkedList<>();
        expResult.add(Pair.of(new Cidade("brasilia", 206.12, -15.7797200, -47.9297200), 19));
        expResult.add(Pair.of(new Cidade("lapaz", 9.70, -16.5000000, -68.1500000), 7));
        expResult.add(Pair.of(new Cidade("assuncao", 6.24, -25.3006600, -57.6359100), 7));
        expResult.add(Pair.of(new Cidade("quito", 14.88, -0.2298500, -78.5249500), 7));

        List<Pair<Cidade, Integer>> expResult2 = new LinkedList<>();
        expResult2.add(Pair.of(new Cidade("brasilia", 206.12, -15.7797200, -47.9297200), 19));
        expResult2.add(Pair.of(new Cidade("bogota", 46.86, 4.6097100, -74.0817500), 11));
        expResult2.add(Pair.of(new Cidade("paramaribo", 0.04, 5.8663800, -55.1668200), 15));

        List<Pair<Cidade, Integer>> result = instanceSmallNet.centralCitiesOrderByHighPopulationDistanceAverage(5, 4);
        List<Pair<Cidade, Integer>> result2 = instanceSmallNet.centralCitiesOrderByHighPopulationDistanceAverage(8, 3);
        System.out.println(result2.toString());

        assertEquals(expResult.toString(), result.toString());
        assertEquals(expResult2.toString(), result2.toString());
    }

    @Test
    public void testShortestPathBetweenUsers() {

    }

}
